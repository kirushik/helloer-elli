defmodule HelloerElli.Supervisor do
  use Supervisor

  def start_link do
    Supervisor.start_link(__MODULE__, :ok)
  end

  def init(:ok) do
    children = [
      worker(:elli, [elli_options])
    ]

    supervise(children, strategy: :one_for_one)
  end

  defp folder do
    System.get_env("FOLDER") || System.cwd
  end

  defp elli_options do
    [
      callback: :elli_middleware,
      callback_args: [
        mods: [
          {:elli_fileserve, [path: folder]}
        ]
      ],
      port: 3000
    ]
  end
end

defmodule HelloerElli do
  use Application

  def start(_type, _args) do
    { :ok, _pid } = HelloerElli.Supervisor.start_link
  end
end

