# HelloerElli

Use [Erlang Solutions](https://packages.erlang-solutions.com/erlang/) packages to install latest Erlang and Elixir releases.

To start the API server:

  * Install dependencies with `mix deps.get`
  * Compile everything for production: `MIX_ENV=prod mix do compile`
  * Start the server: `MIX_ENV=prod mix run --erl "+K true" --no-halt`

The fileserver will serve files from current directory. To change that, pass an *absolute* path
in the `FOLDER` environmental variable.
