defmodule HelloerElli.Mixfile do
  use Mix.Project

  def project do
    [app: :helloer_elli,
     version: "0.0.1",
     elixir: "~> 1.2",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    [
      applications: [:inets],
      mod: {HelloerElli, []}
    ]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
    [
      {:elli, github: "knutin/elli", branch: "master"},
      {:elli_fileserve, github: "chrisavl/elli_fileserve"}
    ]
  end
end
